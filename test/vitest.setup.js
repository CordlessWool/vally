import { vi, beforeAll } from 'vitest'

beforeAll(() => {
  vi.mock('ioredis', () => vi.importActual('ioredis-mock'))
})
