const plugin = require('tailwindcss/plugin');
const { transparent } = require('tailwindcss/colors');
const colors = require('tailwindcss/colors');

module.exports = {
  content: ['./src/**/*.{html,js,svelte,ts}', './content/**/*.json'],
  theme: {
    colors: {
      primary: colors.zinc,
      neutral: colors.neutral,
      success: colors.emerald,
      unvisible: colors.zinc['100'],
      info: colors.blue,
      warning: colors.orange,
      danger: colors.red,
      white: colors.white,
      black: colors.black,
      overlay: {
        text: "#FAFAFACF",
        light: "#EAEAEA4A"
      },
      transparent: colors.transparent,
      current: colors.current,
    },
    extend: {
      width: {
        '128': '32rem',
        '160': '40rem'
      },
      margin: {
        '128': '32rem',
        '160': '40rem'
      }
    },
  },
  plugins: [
    require('@tailwindcss/forms'),
    plugin(function ({addUtilities, addVariant, theme}) {
      addVariant('hocus', ['&:hover', '&:focus'])
    })
  ],
}
