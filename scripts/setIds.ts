import fs from 'fs/promises';
import path from 'path';
import { program } from 'commander';
import { nanoid } from 'nanoid';


export const getJson = async (pathString: string, filename: string) => {
  const dataString = (await fs.readFile(path.join(pathString, filename), 'utf8')).trim();
  return JSON.parse(dataString);
}

export const saveJson = async (pathString: string, filename: string, data: unknown) => {
  return fs.writeFile(path.join(pathString, filename), JSON.stringify(data), 'utf-8')
}

export const getListOfJson = async (basePath: string): Promise<string[]> => {
  return (await fs.readdir(basePath)).filter(name => name.endsWith('.json'));
}


program.parse();



if(!program.args[0]) {
  console.info('please provide a basePath');
  process.exit(1);
}

const basePath = path.resolve(program.args[0]);


const pathsToJsonFiles = await getListOfJson(basePath);

await Promise.all(pathsToJsonFiles.map(async (filePath: string) => {
  const data = await getJson(basePath, filePath);
  if((data.type as string).toUpperCase() === 'TEMPLATE' && typeof data.id !== 'string') {
    data.id = nanoid();
    await saveJson(basePath, filePath, data);
  }
}));




