FROM node:18 as build

ENV BUILD_FOR node
WORKDIR /app

COPY . /app/

#Install packages for building the app
RUN npm ci
RUN npm run build


FROM node:18-alpine
WORKDIR /app

ENV NODE_ENV production
COPY --from=build /app/build /app/build/
COPY --from=build /app/package.json /app/
COPY --from=build /app/package-lock.json /app/
COPY content /app/content
COPY templates /app/templates

#Install packages for production
RUN npm ci --omit=dev

ENTRYPOINT [ "node", "/app/build" ]
