import type { RequestHandler } from './$types';
import fs from 'fs';

export const GET: RequestHandler = () => {

  const jsonString = fs.readFileSync('./content/vally.config.json', 'utf-8')
 
  return new Response(jsonString);
}