import { error, json } from '@sveltejs/kit';
import type { RequestHandler } from './$types';
import type { Template, TemplateMeta } from '@types';
import { TemplateFilesystem } from '@logic/tepmlates';

const manager = new TemplateFilesystem('content/templates');

export const GET: RequestHandler = async ({params, url}) => {
  console.info(`Load template meta data for ${params.category}`)

  const includeSecondary = url.searchParams.get('secondary') === 'true' 

  const templates: Template[] = await manager.find({ 
    categories: params.category,
    secondary: includeSecondary
  })

  const fromCategorie: TemplateMeta[] = templates.map(({component, ...meta}) => meta);

  if(fromCategorie.length === 0) {
    throw error(404, {
      message: "couldn't find templates"
    })
  }

  return json(fromCategorie);

}