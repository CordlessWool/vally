import { JsonFilesystem } from "@lib/content-manager";
import { error, json } from "@sveltejs/kit";
import type { Template } from "@types";
import { caching } from 'cache-manager';
import type { RequestHandler } from './$types';

const memory = await caching("memory");
const CATEGORIES = 'TEMPLATE:CATEGRORIES';

await (async function bootstrap() {
  console.info(`bootstrap categroies`)
  const content = new JsonFilesystem<Template>('content/templates'); // TODO: read path from config
  const templates = await content.find( {} ); // get all elements
  const categorires = templates.reduce<Set<string>>((acc, cur: Template) => {
    return new Set<string>([...acc, ...cur.categories])
  }, new Set<string>());
  await memory.set(CATEGORIES, Array.from(categorires));

})()

export const GET: RequestHandler = async ({params}) => {

  const categories = await memory.get<string[]>(CATEGORIES);

  if(categories == null || categories.length === 0){
    throw error(404, 'No categories found');
  }

  return json(categories);

}