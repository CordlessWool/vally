import { json, error } from "@sveltejs/kit";
import type { RequestHandler } from './$types';
import { TemplateFilesystem } from '@logic/tepmlates';

const manager = new TemplateFilesystem('content/templates');

export const GET: RequestHandler = async ({params}) => {
  const id = params.template;
  console.info(`Load Template for ID ${id}`)
  const [{component}] = await manager.find({id});

  if(component == null) {
    throw error(404);
  }

  return json(component);

}