import { TemplateFilesystem } from "@logic/tepmlates";
import { json } from "@sveltejs/kit";
import type { RequestHandler } from "./$types";


function getFilteredSearchParams (url: URL, allowedParams: string []): Record<string, string> {
  const {searchParams} = url;
  const paramsAsEntries = Array.from(searchParams.entries());
  const filtered = paramsAsEntries.filter(
    ([key]) => allowedParams.includes(key.toLowerCase())
  )
  return Object.fromEntries(filtered);
}

const manager = new TemplateFilesystem('content/templates')

export const GET: RequestHandler = async ({url}) => {

  const searchParams = getFilteredSearchParams(url, ['name']);
  const templates = await manager.find(searchParams);

  return json(templates || []);

}