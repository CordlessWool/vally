import { Filesystem } from "@lib/content-manager";
import { error } from "@sveltejs/kit";
import type { RequestHandler } from "./$types"

const manager = new Filesystem('content/templates');

export const GET : RequestHandler = async ({params}) => {
  
  const image = await manager.load(params.slug);

  if(image == null) {
    throw error(404)
  }

  return new Response(image)
}