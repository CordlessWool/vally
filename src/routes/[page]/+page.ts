import { PUBLIC_BUILD_MODE } from "$env/static/public";

export const prerender = true;
export const ssr = true;
export const csr = PUBLIC_BUILD_MODE?.toUpperCase() === 'NODE';