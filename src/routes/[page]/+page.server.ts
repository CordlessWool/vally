import { hasPage } from "@logic/filesystem";
import { error } from "@sveltejs/kit";
import type { PageServerLoad } from "./$types"
import { prepareCompute } from "../../logic/compute";
import { JsonFilesystem } from "@lib/content-manager";
import { MODES, type HeaderFooterFile, type PageData, type PageFile } from "@types";
import { building } from "$app/environment";

const pages = new JsonFilesystem<PageFile>('content/pages');
const headers = new JsonFilesystem<HeaderFooterFile>('content', {postfix: 'header'})
const footers = new JsonFilesystem<HeaderFooterFile>('content', {postfix: 'footer'})

function getMode (url: URL): MODES | undefined {
  if(building === true) {
    return MODES.VIEW;
  } else if (url.searchParams.has('pageframe')) {
    return MODES.PAGEFRAME;
  } else if (url.searchParams.has('edit')) {
    return MODES.EDIT;
  } else if (url.searchParams.has('preview')) {
    return MODES.PREVIEW;
  } else {
    return MODES.VIEW;
  }
}


export const load: PageServerLoad = async ({ params, url, fetch }) => {
  const {page} = params;
  console.info('Start loading page data: ', page);
  
  if(!(await hasPage(page))){
    console.warn('No page data found');
    throw error(404)
  }
  const {footer: footerRef,  header: headerRef, ...data} = await pages.load(page);
  const mode = getMode(url)
  console.info(`Editmode: ${mode}`);

  if(mode === MODES.PREVIEW) {
    return {
      ...data,
      content: (await Promise.all(data.content.slice(0, 2).map(prepareCompute({fetch})))),
      mode: MODES.PREVIEW
    }   
  } else {

    const footer = await footers.load(footerRef || 'default');
    const header = await headers.load(headerRef || 'default');

    let manipulatedData: Partial<PageData> = {};

    if(mode === MODES.EDIT || mode === MODES.VIEW) {
      manipulatedData = {
        ...manipulatedData,
        header: {
          ...header,
          content: (await Promise.all(header.content.map(prepareCompute({fetch}))))
        },
        footer: {
          ...footer,
          content: (await Promise.all(footer.content.map(prepareCompute({fetch}))))
        }
      }
    }

    if(mode === MODES.PAGEFRAME || mode === MODES.VIEW) {
      manipulatedData = {
        ...manipulatedData,
        content: (await Promise.all(data.content.map(prepareCompute({fetch}))))
      }
    }

    console.info('Send page data');
    return {
      ...data,
      header,
      footer,
      ...manipulatedData,
      mode
    };


  }
  

  
}
