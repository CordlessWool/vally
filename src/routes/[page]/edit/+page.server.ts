import { JsonFilesystem } from "@lib/content-manager";
import type { PageServerLoad } from "../$types";

const manager = new JsonFilesystem('content/pages');

export const load: PageServerLoad = async ({params}) => {
  const {page} = params;
  try {
    const {title, slug, lang, description, author} = await manager.load(page);
    return {name: page, title, slug, lang, description, author};
  } catch (err) {
    console.error(err);
    throw err;
  }
}