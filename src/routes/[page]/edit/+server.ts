import { deletePage, getListOfPages, hasPage, savePage } from "@logic/filesystem";
import { error, json, redirect } from "@sveltejs/kit";
import type { Action } from "./$types";
import type { HeaderFooterFile, PageData, PageFile } from "@types";
import { JsonFilesystem } from "@lib/content-manager";


const pageManager = new JsonFilesystem<PageFile>('content/pages')
const headerManager = new JsonFilesystem<HeaderFooterFile>('content', {postfix: 'header'})
const footerManager = new JsonFilesystem<HeaderFooterFile>('content', {postfix: 'footer'})

function hasValue(value: string | undefined) {
  if(value === null) {
    return false;
  } else if(typeof value === 'string' && value.trim() === ""){
    return false;
  }

  return true
}

function validateRequest(data: Partial<PageData>, page: string) {
  const {title, slug} = data;

  if(!hasValue(title)){
    console.warn('Missing props')
    return error(400, 'slug of main is not allowed to be changed')
  }

  if(slug != null && (page === 'main' && slug !== '/')) {
    console.warn('Slug change on Main')
    throw error(400, 'slug of main is not allowed to be changed')
  } 
}


export const POST: Action = async ({params, request}) => {
  console.info('Creating a new page')
  if((await hasPage(params.page))) {
    return error(400, 'Page already exists')
  }

  const {title, lang = 'en'} = (await request.json()) as Partial<PageData>;
  
  if(title == null || title.trim() === "") {
    return error(400, 'missing at least one propterty') 
  }

  const newPage: PageFile = {
    type: 'Page',
    lang,
    slug: title.trim().replaceAll(' ', '-'),
    title: title.trim(),
    content: []
  }

  savePage(params.page, newPage);

  return redirect(307, `/build/pages/${params.page}`);
}

export const PUT: Action = async ({params, request}) => {
  console.info(`Updating page data from ${params.page}`)
  if(await hasPage(params.page)) {
    const data = await request.json() as Partial<PageData>;
    
    await validateRequest(data, params.page);

    const {footer, header, ...page} = data

    if(footer) {
      await footerManager.put(footer.name, {
        ...footer
      }, true);
    }

    if(header) {
      await headerManager.put(header.name, {
        ...header
      }, true);
    }

    
    if(Object.keys(page).length !== 0) {
      await pageManager.put(params.page, page, true)
    }
    console.info('Page was updated')
    return json({success: true})

  }

  throw error(404)
}

export const DELETE: Action = async ({params}) => {
  try {
    const {page} = params;
    console.info(`Deleting page ${page}`)
    if(page === 'main'){
      return error(403);
    }

    const pages = await getListOfPages();
    const pageNames = pages.map((page) => page.slice(0, -5))
    const currentIndex = pageNames.indexOf(page);

    const nextPage = 
      currentIndex === pageNames.length-1 
      ? pageNames[currentIndex-1]
      : pageNames[currentIndex+1];

    
    deletePage(page);
    return redirect(307, `/build/pages/${nextPage}`) 
  } catch(err) {
    console.error(err);
    return error(400)
  } 
}