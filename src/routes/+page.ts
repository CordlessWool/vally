import type { PageLoad } from "./$types";  
import { PUBLIC_BUILD_MODE } from "$env/static/public";

export const prerender = true;
export const ssr = true;
export const csr = PUBLIC_BUILD_MODE?.toUpperCase() === 'NODE';
export const load: PageLoad = async ({ fetch, url }) => {
  const main = new URL(url);
  main.pathname = 'main';
  const response = await fetch(main);

  // const {type, ...pageData}: PageData = (await response.json());

  return {
    page: await response.text()
  }
}
