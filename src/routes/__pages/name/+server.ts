import { JsonFilesystem } from "@lib/content-manager";
import { json } from "@sveltejs/kit";
import type { PageFile, PageInfo } from "@types";
import type { RequestHandler } from "./$types";

const manager = new JsonFilesystem<PageFile>('content/pages');



export const GET: RequestHandler = async () => {
  const allPageData = await manager.find({});
  const pages: PageInfo[] = allPageData.map(({title, slug}) => ({title, slug})) 
  return json(pages);
}