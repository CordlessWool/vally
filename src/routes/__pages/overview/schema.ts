import {object, string} from 'yup';

export const createSchema = object({
  type: string().default('Page').oneOf(['Page']),
  lang: string().length(2).default('en'),
  slug: string().trim().required().transform((value) => encodeURI(value.replaceAll(' ', '-'))),
  title: string().trim().required(),
  description: string().trim()
})