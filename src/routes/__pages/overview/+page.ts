import type { PageLoad } from "./$types";

export const prerender = false;

export const load: PageLoad = async ({fetch}) => {
  const resposen = await fetch('/__pages/name');
  const pages = resposen.json();
  return {
    pages
  }
}