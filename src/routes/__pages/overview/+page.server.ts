import { JsonFilesystem } from "@lib/content-manager";
import { fail } from "@sveltejs/kit";
import type { PageFile, PageData } from "@types";
import type { Actions } from "./$types";
import { createSchema } from "./schema";

const manager = new JsonFilesystem('content/pages');

export const actions: Actions = {
  create: async ({request}) => {
    console.info('Creating a new page')
    const formData = await request.formData();
    try {
      
  
      const data = <PageData>(Object.fromEntries(Array.from(formData.entries())) as unknown);
      await createSchema.validate(data, {
        strict: true,
        stripUnknown: true
      });
      const { slug } = data;

      if(await manager.has(slug)){
        return fail(400, {type: 'Already Exists'})
      }

      

      const newPage: PageFile = {
        ...data,
        footer: 'default',
        header: 'default',
        content: []
      }

      await manager.put(slug, newPage);
      console.info(`page was successfull saved at: ${slug}`);
      return { success: true }
    } catch (err) {
      console.error(err);
      return fail(400);
    }
  },
  
}