import { saveFile, saveImage } from "@logic/filesystem";
import { redirect } from "@sveltejs/kit";
import type { RequestHandler } from './$types';


export const GET:RequestHandler = async ({url}) => {
  console.info('Load image via load property')
  const source = url.searchParams.get('load')
  if(source) {
    throw redirect(307, source)
  }

  return new Response(undefined, { status: 404 })
}

export const POST: RequestHandler = async ({params, request}) => {
  const {type} = params;
  console.info(`Starting ${type} upload`)
  const data = await request.formData();
  let upload: File | undefined;
  data.forEach((file: FormDataEntryValue) => {
    if(typeof file !== 'string') {
      upload = file;
    }
  })

  if(upload == null) {
    console.warn(`Failed ${type} upload with BadRequest`)
    return new Response(undefined, { status: 400 })
  }

  const writeFu = type === 'images' ? saveImage : saveFile;

  const fileName = await writeFu(upload);
  
  console.info(`Finsih ${type} upload`)

  return new Response(`/${type}/${fileName}`, { status: 201 })
}