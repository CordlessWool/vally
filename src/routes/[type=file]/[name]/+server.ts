import { Filesystem } from "@lib/content-manager";
import { error, type RequestEvent } from "@sveltejs/kit";
import sharp from "sharp";
import type { RequestHandler, RouteParams } from './$types';

const imageManager = new Filesystem('content/images');
const fileManager = new Filesystem('content/files');

const allowedFormats = ['webp', 'jpeg', 'jpg', 'png', 'tiff', 'tif', 'gif'] as const
type allowedFormatsType = typeof allowedFormats[number]

function transformImage (image: Buffer, params: [string, string][]): Promise<Buffer> {
  if(params.length === 0) {
    return Promise.resolve(image);
  }

  return params.reduce((acc, [key, value]) => {
    if(key === "fm") {
      if(!allowedFormats.includes(value as allowedFormatsType)){
        throw error(400, 'value for fm is not supported')
      }
      const q = params.find(([key]) => key === 'q');
      return acc.toFormat((value as allowedFormatsType), {
        quality: Array.isArray(q)? Number(q[1]) : 80,
        effort: 6
      })
    } else if (key === 'width') {
      return acc.resize(Number(value))
    }
    
    return acc;
  }, sharp(image)).toBuffer()

}

export async function _fetchImageAtBuild (url: string | URL): Promise<Buffer> {
  url = new URL(url);
  const params = {
    params: {
      type: 'images',
      name: url.pathname.replace('/images/', '')
    },
    url
  };

  const response = await GET(params as RequestEvent<RouteParams, "/[type=file]/[name]">);
  return Buffer.from(await response.arrayBuffer());
}



export const GET: RequestHandler = async ( {params, url}) =>  {
  const { name, type } = params;
  console.info(`Load ${type} via api`);

  let file;

  if(type === 'images'){
    const searchParams = Array.from(url.searchParams.entries());
    const image: Buffer = Buffer.from(await imageManager.load(name)); 
    file = await transformImage(image, searchParams);
  } else {
    file = await fileManager.load(name);
  }

  return new Response(file);
}

