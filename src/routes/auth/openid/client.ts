import { env} from '$env/dynamic/private';
import { PUBLIC_SERVER_URL } from '$env/static/public';
import type { SessionObject } from '@logic/sessions';
import { Issuer, type TokenSetParameters } from 'openid-client';

export const client = env.OPENID_PROVIDER_URL == null ? undefined :
  new (await Issuer.discover(env.OPENID_PROVIDER_URL)).Client({
      client_id: env.OPENID_CLIENT_ID || "",
      client_secret: env.OPENID_CLIENT_SECRET,
      redirect_uris: [`${PUBLIC_SERVER_URL}/auth/openid`],
      response_types: ['code']
  })

export const isAuthenticated = async (session: SessionObject) => {
  if(!env.OPENID_PROVIDER_URL || env.OPENID_PROVIDER_URL.length === 0){
    return true;
  }

  const {auth} = session;
  if(auth?.expires_at && auth.expires_at > Date.now()){
    return true;
  }

  return false;
}


export const refreshToken = async (session: SessionObject) => {
  try {
    const {auth} = session;
    if (auth?.refresh_token && auth?.refresh_at && auth.refresh_at < Date.now()) {
      const tokenSet = await client.refresh(auth.refresh_token);
      addTockenSetToSesseion(session, tokenSet)
    }
  } catch (err) {
    console.warn('Refreshing Token failed')
    console.warn(JSON.stringify(err));
    //TODO: handle errors
  }
}

export const addTockenSetToSesseion = (session: SessionObject, tokenSet: TokenSetParameters) => {
  const timestamp = Date.now();
  if(!tokenSet.expires_at) throw new Error('Authentication provider have to send an expires at')
  tokenSet.expires_at = tokenSet.expires_at * 1000;
  tokenSet.refresh_at = timestamp + (Math.floor((tokenSet.expires_at - timestamp)/2));
  session.auth = tokenSet;
}
