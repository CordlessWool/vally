import { redirect } from '@sveltejs/kit';
import { PUBLIC_SERVER_URL } from '$env/static/public'
import type { RequestHandler } from './$types';
import { addTockenSetToSesseion, client } from './client';

export const GET: RequestHandler = async ({url, locals}) => {
  console.info('Logging in with openID')

  const {codeVerifier} = locals.session;

  if(codeVerifier){
    // const session_state = url.searchParams.get('session_state');//client.callbackParams(url.searchParams.toString());
    const params = Object.fromEntries(url.searchParams.entries())
    const tokenSet = await client.callback(`${PUBLIC_SERVER_URL}/auth/openid`, params, { code_verifier: codeVerifier });
    addTockenSetToSesseion(locals.session, tokenSet)
    console.info('logged in with openID')

  }
  throw redirect(307, '/build')
}