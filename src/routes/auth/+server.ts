import type { RequestHandler } from './$types';


import { generators } from 'openid-client';
import { error, redirect } from '@sveltejs/kit';
import { client } from './openid/client'

export const GET: RequestHandler = ({locals}) => {
  console.info('Auth user')
  const codeVerifier = generators.codeVerifier();
  // store the code_verifier in your framework's session mechanism, if it is a cookie based solution
  // it should be httpOnly (not readable by javascript) and encrypted.
  
  const codeChallenge = generators.codeChallenge(codeVerifier);
  
  if(client == null) {
    throw error(500);
  }

  const url = client.authorizationUrl({
    scope: 'openid email profile',
    //redirect_uri: encodeURI('http://localhost:5173/auth/openid'), //TODO: redirect to called url
    code_challenge: codeChallenge,
    code_challenge_method: 'S256',
  });

  locals.session = {
    ...locals.session,
    codeVerifier
  }

  throw redirect(307, url)
}