import type { TextEditOptions } from "./types";


export const headline: TextEditOptions = {
  headlines: [1] 
}

export const text: TextEditOptions = {
  headlines: [2, 3, 4]
}