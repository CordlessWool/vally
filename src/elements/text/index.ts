import Component from "./text.svelte";
import ConstructionKit from "./text.edit.svelte";

export const identifier = "Text";

export { Component, ConstructionKit }