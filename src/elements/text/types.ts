import type { ElementData } from "@elements/types";
import type {Level} from '@tiptap/extension-heading'
export interface TextEditOptions {
  headlines?: Level[]
}

export interface TextElement extends ElementData {
  content: string;
  design: 'custom' | 'default';
  id: string;
  class: string;
  editProfile?: 'headline' | 'text';
  options?: TextEditOptions;
}