import Component from "./component.svelte";
import ConstructionKit from "./constructionkit.svelte";

export const identifier = 'Mapbox';

export {Component, ConstructionKit}