import type { ElementData } from "@elements/types";

export interface MapElement extends ElementData {
  token: string;
  class: string;
  style: string;
  lat: number;
  lon: number;
  zoom: number;
  alt?: string;
  height?: number;
  width?: number;
} 