export interface LinkElement {
  id: string;
  icon?: string;
  name: string;
  link: string;
  class: string;
  target?: '_blank' | '_self';
  color?: string;
  colorOnlyIcon: boolean;
  design?: "list" | "icon-focused";
}

export type LinkModal 
  = Pick<LinkElement, 'icon' | 'name' | 'color' | 'colorOnlyIcon' | 'link' | 'target'>