export enum LINK_TYPE {
  EXTERN,
  PHONE,
  EMAIL,
  SKYPE,
  FAX,
  SMS,
  PAGE
}

export class Link {

  #type: LINK_TYPE;
  #prefix: string;
  #value: string;
  static linkPrefix: [LINK_TYPE, string][] = [[LINK_TYPE.EXTERN, 'Link'],[LINK_TYPE.PHONE, 'tel:'], [LINK_TYPE.EMAIL, 'mailto:'], [LINK_TYPE.SKYPE, 'callto:'], [LINK_TYPE.FAX, 'fax:'], [LINK_TYPE.FAX, 'sms:'] ];
	static refOptions: [LINK_TYPE, string][] = [[LINK_TYPE.EXTERN, 'Link'],[LINK_TYPE.PHONE, 'Phone'], [LINK_TYPE.EMAIL, 'E-Mail'], [LINK_TYPE.SKYPE, 'Skype'], [LINK_TYPE.FAX, 'Fax'], [LINK_TYPE.SMS, 'SMS'] ];

  constructor(link = "") {
    if(link.startsWith('/')) {
			this.#type = LINK_TYPE.PAGE;
      this.#prefix = "";
      this.#value = link;
		} else {
      const type = Link.linkPrefix.find(([, ref]) => link.startsWith(ref))
      if(type == null) {
        this.#type = LINK_TYPE.EXTERN
        this.#prefix = "";
        this.#value = link;
      } else {
        const [t, prefix] = type
        this.#type = t;
        this.#prefix = prefix;
        this.#value = link.replace(prefix, "").trim();
      }
    } 
  }

  get prefix (): string {
    return this.#prefix;
  }

  set type (type: LINK_TYPE) {
    [this.#type, this.#prefix] = Link.linkPrefix.find(([t,]) => t === type) || [type ,""];
  }

  get type (): LINK_TYPE {
    return this.#type;
  }

  set value (value: string) {
    this.#value = value;
  }

  get value(): string {
    return this.#value;
  }

  toString (): string {
    return `${this.#prefix}${this.#value}`
  }

} 