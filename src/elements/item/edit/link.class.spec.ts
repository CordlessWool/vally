import {Link, LINK_TYPE} from './link.class';


describe('Link Class in Element', () => {
  it('detect internal page link', () => {
    const slug = '/internal-page'
    const link = new Link(slug);

    expect(link.toString()).toBe(slug);
    expect(link.type).toBe(LINK_TYPE.PAGE);
    expect(link.prefix).toBe("");
  })

  it('detect internal phone', () => {
    const slug = 'tel:+12393412312'
    const link = new Link(slug);

    expect(link.toString()).toBe(slug);
    expect(link.type).toBe(LINK_TYPE.PHONE);
    expect(link.prefix).toBe("tel:");
  })

  it('detect internal page link', () => {
    const slug = 'ftp://link-to-external-source.test'
    const link = new Link(slug);

    expect(link.toString()).toBe(slug);
    expect(link.type).toBe(LINK_TYPE.EXTERN);
    expect(link.prefix).toBe("");
  })

  it('detect internal page link', () => {
    const slug = 'mailto:mail@mecum.website'
    const link = new Link(slug);

    expect(link.toString()).toBe(slug);
    expect(link.type).toBe(LINK_TYPE.EMAIL);
    expect(link.prefix).toBe("mailto:");
  })

  it('detect internal page link', () => {
    const value = 'mail@mecum.website'
    const slug = `mailto:${value}`
    const link = new Link(slug);
    link.type = LINK_TYPE.PHONE

    expect(link.toString()).toBe(`tel:${value}`);
    expect(link.type).toBe(LINK_TYPE.PHONE);
    expect(link.value).toBe(value);
    expect(link.prefix).toBe("tel:");
  })

  it('detect internal page link', () => {
    const prefix = 'tel:'
    const newValue = '0711/823123'
    const oldValue = '0123827743423'
    const slug = `${prefix}${oldValue}`
    const link = new Link(slug);

    expect(link.toString()).toBe(`tel:${oldValue}`);
    expect(link.type).toBe(LINK_TYPE.PHONE);
    expect(link.value).toBe(oldValue);
    expect(link.prefix).toBe("tel:");


    link.value = newValue

    expect(link.toString()).toBe(`tel:${newValue}`);
    expect(link.type).toBe(LINK_TYPE.PHONE);
    expect(link.value).toBe(newValue);
    expect(link.prefix).toBe("tel:");
  })
})