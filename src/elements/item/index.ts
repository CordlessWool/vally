import Component from './item.svelte';
import ConstructionKit from './item.edit.svelte';

export const identifier = "Item";

export {Component, ConstructionKit};