import { Subject, switchMap, startWith, debounceTime, distinctUntilChanged } from "rxjs";


export function createSearch<T = unknown> (sourceFuntion: (text:string) => Promise<Array<T>>) {
  const search$ = new Subject();
  const _items$ = search$.pipe(
    debounceTime(500),
    distinctUntilChanged(),
    switchMap(text => sourceFuntion(<string>text)),
    startWith([])
  )

  return {
    search: (text: string) => search$.next(text),
    subscribe: _items$.subscribe.bind(_items$)
  }
}