import type { ElementLogic } from "../types";

import type { ElementData } from "../types";
import Component from './component.svelte';
import ConstructionKit from "./constructionkit.svelte";

export const identifier = 'Group';

export {Component, ConstructionKit};


export interface dataType extends ElementData {
  content: ElementData[]
}

export const beforeBuild: ElementLogic['beforeBuild']  = async (data, {compute}) => {
  const content = await Promise.all((data as dataType).content.map(compute));

  return {
    ...data,
    content: content
  }
}