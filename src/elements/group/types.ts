import type { ElementData } from '@elements/types';


export interface GroupElement {
  id: string;
  width: 'full' | 'default';
  element: string;
  class: string;
  design: 'custom' | 'default';
  content: ElementData[];
  extendable: string[];
}