import type {Element} from './types';

import DefaultElementEditor from '@comp/element/default.element.editor.svelte';

import * as text from '@elements/text';
import * as image from '@elements/image'
import * as item from '@elements/item';
import * as map from '@elements/map'
import * as group from '@elements/group';
const elementList: Element[] = [text, image, item, map, group];

const elements: Record<string, Element> = {
  
}

elementList.forEach ((element) => {
  elements[element.identifier] = element;
}); 


// console.info('Loading elements...')
// const elementsFolder = './src/elements/'
// fs.readdirSync(elementsFolder, {withFileTypes: true}).map(async (dir) => {
//   if(dir.isDirectory()) {
//     const { identifier, ...element } = await import(`./${dir.name}/index.ts`)
    
//     if(identifier != null) {
//       console.info(` - ${identifier}`);
//       elements[identifier] = element;

//     }
//   }
// })

type elementKeys = keyof typeof elements;

export {elements};

export function getElement(elementName: elementKeys) {
  if(!Object.hasOwn(elements, elementName)){
    throw new Error(`Could not compile, element ${elementName} is not install`); //TODO: Complie error
  }
  return elements[elementName]
}

export function getComponentFor(elementName: elementKeys): Element["Component"] | undefined {
  try {
    return elements[elementName].Component
  } catch (err) {
    console.error(`Could not load compoennt ${elementName}`);
    console.error(err);
  }
}

export function getConstructionKitFor(elementName: elementKeys): Element["ConstructionKit"] {
  return elements[elementName]?.ConstructionKit || DefaultElementEditor;
}
