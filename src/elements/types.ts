import type { JsonMap } from '@types';
import type { SvelteComponent } from 'svelte';
import type { compute, storeAsset } from '../logic/compute'

export type HandlerAttributeTye = string | {
  name: string;
  saveTo: string;
}

interface BeforeBuildOptions {
  compute: typeof compute
  storeAsset: typeof storeAsset
  fetch: typeof fetch
}

export interface ElementData extends JsonMap{
  type: string;
  id?: string;
  template?: string;
  [x: string]: unknown;
}

type ReStUn = ElementData;

export interface Element extends ElementLogic {
  identifier: string;
  Component: typeof SvelteComponent;
  ConstructionKit?: typeof SvelteComponent;
}
export interface ElementLogic {
  beforeBuild?: (data: ReStUn, {compute}: BeforeBuildOptions) => Promise<ReStUn>,
}

export interface ImageData {
  src: [string, number][];
  srcset: [string, number][];
  alt: string;
}