import { ImageUrlContstructor } from "./ImageUrlConstructor";


describe( 'Test url generation', () => {
  describe('Unsplash', () => {
    it('fromFormat', () => {
      const params = {
        sizes: [300, 700],
        quality: 70,
        format: 'webp'
      }
      const unsplashUrl = "https://images.unsplash.com/photo-1585212156846-ab9106dca29c?ixid=MnwzNDQxMjN8MHwxfHNlYXJjaHwyfHx0fGVufDB8fHx8MTY1NzI5ODMxOQ&ixlib=rb-1.2.1/images/teaser.jpg"
      const construcor = ImageUrlContstructor.fromFormat(unsplashUrl, params)

      const {urls} = construcor;
      const sizes = [...params.sizes]
      urls.forEach((url) => {
        const index = sizes.findIndex((size) => size === Number(url.searchParams.get('width')));
        const match = sizes.splice(index, 1);
        expect(match.length).toBeGreaterThan(0);
        expect(Object.fromEntries(url.searchParams.entries())).toContain({
          width: match[0].toString(),
          q: params.quality.toString(),
          fm: params.format
        })
      })

      expect(sizes.length).toBe(0);

    })
  })
})