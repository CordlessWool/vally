import { PUBLIC_SERVER_URL } from "$env/static/public";
import type { ImageFormatOptions } from "./types";

export class ImageUrlContstructor {

  #urls: URL[];
  #fallback: URL;
  #url: URL;

  constructor(url: string | URL) {
    this.#url = new URL(url, PUBLIC_SERVER_URL);
    this.#fallback = this.#url;
    this.#urls = [this.#url];
  }

  private setUrlParam (param: string, value: string) {
    this.#urls.forEach((url) => {
      url.searchParams.append(param, value);
    })
  }

  setWidths(widths: number []) {
    const param = 'width';
    this.#urls = this.#urls.map((url) => widths.map((size) => {
      const version = new URL(url);
      version.searchParams.append(param, size.toString());
      return version
    })).flat();
  }

  setQuality(quality: number) {
    this.setUrlParam('q', quality.toString());
  }

  setFormat(format: string) {
    this.setUrlParam('fm', format);
  }

  getFallbackFormat(): string {
    const format = this.#fallback.pathname.split('.').pop()
    if(format == null) {
      throw new Error('fallback image time could not be detected')
    }
    return format;
  }

  getFormat(): string {
    const format = this.#urls[0].searchParams.get('fm');
    if(format == null) {
      return this.getFallbackFormat();
    } else {
      return format;
    }
  }

  get fallback () {
    return this.#fallback;
  }

  get urls () {
    return this.#urls;
  }

  static fromFormat(url: string | URL, format: ImageFormatOptions): ImageUrlContstructor {
    const constructor = new ImageUrlContstructor(url);
    const { sizes, quality, format: imageFormat } = format
    if(sizes?.length > 0) {
      if (!Array.isArray(sizes[0])) {
        constructor.setWidths(sizes as number[]);
      }
    }
    constructor.setQuality(quality || 80);
    constructor.setFormat(imageFormat || 'webp')


    return constructor;  
  }


}