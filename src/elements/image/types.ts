import type { ElementData } from "@elements/types";


export interface BuildLinks {
  type: 'version' | 'fallback',
  link: string,
  original: string
}
export interface ImageFormatOptions {
  sizes: Array<number> | [number, number][]
  quality: number
  format: string
}

export interface ImageElement extends ElementData {
  id: string;
  class: string;
  image: string;
  caption: string;
  alt: string;
  width: 'full' | 'large' | 'normal';
  high: 'full' | 'normal';
}

export interface ImageRenderElement extends ImageElement {
  links: BuildLinks[]
}

export interface ImageElementData extends ElementData {
  format: ImageFormatOptions;
  image: string;
  caption: string;
  alt: string;
}

export interface UnsplashData {
  image: string;
  photograph: string;
  alt?: string;
  caption?: string;
  class?: string;
}

export interface EditData {
  image: string | undefined,
  alt: string,
  caption: string
}