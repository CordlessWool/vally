import { createSearch } from "@elements/utils/search";
import { createApi } from "unsplash-js";

const api = createApi({
  // Don't forget to set your access token here!
  // See https://unsplash.com/developers
  accessKey: "IodJGkxaJOoajaX681rxrKoJMEp7oMd4yxFIGmJN83A"
});

async function getUnsplashPhotos (query: string) {
  const res = await api.search.getPhotos({query});
  if(res.type === "success") {
    return res.response.results;
  }
  
  return [];
}

export function createUnsplashSearch() {

  return createSearch(getUnsplashPhotos);
}
