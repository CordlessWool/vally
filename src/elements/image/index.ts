import type { ElementLogic } from "../types";
import ConstructionKit  from './construct.svelte';
import Component from './component.svelte';
import type { ImageElementData as ElementData } from "./types";
import { ImageUrlContstructor } from "./ImageUrlConstructor";
import { escape } from "svelte/internal";
import { nanoid } from "nanoid";

export const identifier = 'Image'

export const beforeBuild: ElementLogic['beforeBuild'] = async (data, {storeAsset, fetch}) => {
  const { _fetchImageAtBuild } = await import("../../routes/[type=file]/[name]/+server")

  const {image, format, caption, alt} = (data as ElementData);
  const urlConstructor = ImageUrlContstructor.fromFormat(image, format);
  const {urls, fallback} = urlConstructor;
  let links = [];

  if(image.startsWith('/images')) {
    const name = escape((alt || caption || nanoid()).replaceAll(' ', '-'))//TODO: generate name from alt or title (need to be escaped for it)
    urls.push(fallback);
    links = await Promise.all(urls.map(async (url) => {
      // const response = await fetch(url.toString().replace(PUBLIC_SERVER_URL, ''));
      const image: Buffer = await _fetchImageAtBuild(url); // replace with svelteKit fetch
      if(url === fallback) {
        return {
          type: 'fallback',
          link: await storeAsset(image, name, urlConstructor.getFallbackFormat()),
          original: url.toString()
        }
      } else {
        return  {
          type: 'version',
          link: await storeAsset(image, name, urlConstructor.getFormat()),
          original: url.toString()
        }
        
      }
    }))
  } else {
    links = [{
      type: 'fallback',
      link: fallback.toString(),
      original: fallback.toString()
    }, ...urls.map((url) => ({
      link: url.toString(),
      type: 'version',
      original: url.toString()
    }))]
  }
  
  return {
    ...data,
    links
  }
}


export {Component, ConstructionKit, ElementData}
