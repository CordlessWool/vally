/// <reference types="@sveltejs/kit" />

// See https://kit.svelte.dev/docs/types#app
// for information about these interfaces
declare namespace App {
	interface Locals {
		session: import("@logic/sessions").SessionObject
		user?: import("@types").User
	}
	// interface Platform {}
	// interface Session {}
	// interface Stuff {}
}
