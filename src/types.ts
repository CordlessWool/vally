import type { ElementData } from "@elements/types";
import type { Writable } from "svelte/store";

export type SimpleObject = Record<string | number, unknown>


export interface User {
  name: string;
  mail: string;
}

export declare type JsonMap = {
  [key: string]: AnyJson | undefined | unknown;
};

export declare type JsonArray = Array<AnyJson>;

export type AnyJson = string | boolean | number | JsonMap | JsonArray;


export enum MESSEGE_EVENT_TYPES {
  CONTENT,
  FOOTER,
  HEADER
}

export enum CONTEXT {
  DND,
  TAB_ACTIVE
}

export type DndContextRemoveHandlerCallbackFunction = (id: string) => void
export interface DndContextInterface {
  delete: Writable<string | null>;
}


/**********
 * 
 * Template
 * 
 */

export interface Template extends TemplateMeta {
  component: TemplateBlueprint;
}

export interface TemplateMeta extends JsonMap {
  id: string;
  type: string;
  thumbnail?: string;
  name: string;
  categories: string[];
  base?: boolean; // TODO: what was it for?
  secondary?: boolean; // not useable on top level TODO: renameing
  short: string, //short description to show below image
  description: string;
  extendable?: boolean;
}

export type TemplateBlueprint = ElementData;


export interface PageFile extends JsonMap {
  type: 'Page',
  title: string,
  slug: string,
  lang: string,
  description?: string,
  author?: string,
  content: ElementData[];
  footer?: string
  header?: string
}

export interface PageData {
  type: 'Page',
  title: string,
  slug: string,
  lang: string,
  description?: string,
  author?: string,
  content: ElementData[];
  footer?:  HeaderFooter;
  header?: HeaderFooter;
}

export interface HeaderFooter {
  type: "footer" | "header",
  name: string,
  content: ElementData[]
}

export interface HeaderFooterFile extends HeaderFooter, JsonMap {}

export type PageInfo = Pick<PageFile, 'title' | 'slug'>

export interface PageDataExtended extends PageData {
  mode?: MODES;
}

export interface PageEditMeta {
  name: string
  title: string,
  slug: string,
  lang: string,
  description?: string,
  author?: string,
}

export enum MODES {
  VIEW,
  PREVIEW,
  EDIT,
  PAGEFRAME
}