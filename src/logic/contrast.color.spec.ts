import {contrastColor} from './contrast.color'

describe('Contrast Color', () => {
  it('background: white', () => {
    const contrast = contrastColor({ value: '#FFF'});
    expect(contrast).toBe('black');
  })

  it('background: black', () => {
    const contrast = contrastColor({ value: '#000'});
    expect(contrast).toBe('white');
  })

  it('background: #61788E', () => {
    const contrast = contrastColor({ value: '#61788E'});
    expect(contrast).toBe('white');
  })

  it('background: #6F8F61', () => {
    const contrast = contrastColor({ value: '#6F8F61'});
    expect(contrast).toBe('white');
  })

  it('background: #4FAC27', () => {
    const contrast = contrastColor({ value: '#4FAC27'});
    expect(contrast).toBe('black');
  })

  it('background: #AB2727', () => {
    const contrast = contrastColor({ value: '#61788E'});
    expect(contrast).toBe('white');
  })


  it('background: #DBAAAA', () => {
    const contrast = contrastColor({ value: '#DBAAAA'});
    expect(contrast).toBe('black');
  })


})
