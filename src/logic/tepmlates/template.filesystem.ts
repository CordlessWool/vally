import { Filesystem, JsonFilesystem } from "@lib/content-manager";
import type { Template } from "@types";
import {createHash} from 'crypto'

export class TemplateFilesystem extends JsonFilesystem<Template> {

  protected filemanager: Filesystem;

  constructor(basePath: string) {
    super(basePath);
    this.filemanager = new Filesystem(basePath);
  }

  override async load(identifier: string): Promise<Template> {
      const data = await super.load(identifier);
      
      if(data.thumbnail == null) {
        const imageIdent = await (await this.filemanager.listOfIdentifiers()).find((elem) => elem.endsWith(`${identifier}.png`));
        if(imageIdent != null) {
          data.thumbnail = `/templates/thumbnails/${imageIdent}`;
        }
      }

      if(!data.id) {
        data.id = createHash('sha1').update(identifier).digest('hex');
      }

      return data;
  }
}