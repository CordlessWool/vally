import { storage } from './mermory';
import { nanoid } from 'nanoid';
import type { Cookies } from '@sveltejs/kit';
import type { TokenSetParameters } from 'openid-client';
import { SessionException } from './expection';
import type { SimpleObject } from '@types';
import { SESSION_TTL } from '$env/static/private';

export {SessionException};

export const SESSION_COOKIE_NAME = 'session'
export const SESSION_TTL_AS_NUMBER = Number(SESSION_TTL || 3600)

function getId (cookieOrId: Cookies | string) : string {
  if(typeof cookieOrId === 'string'){
    return cookieOrId;
  } else {
    return cookieOrId.get(SESSION_COOKIE_NAME) || nanoid();
  }
}

export type SessionObject = SimpleObject & {
  auth?: TokenSetParameters
}


function setSessionCookie (cookie: Cookies, id: string) {
  cookie.set(SESSION_COOKIE_NAME, id, {
    httpOnly: true,
    maxAge: SESSION_TTL_AS_NUMBER
  })
}

export function getSession (id: string): SessionObject
export function getSession (cookie: Cookies): SessionObject
export function getSession (cookieOrId: Cookies | string): SessionObject {
  const id = getId(cookieOrId);

  if(typeof cookieOrId === 'object' && Object.hasOwn(cookieOrId, 'set')) {
    setSessionCookie(cookieOrId, id); //can not be done after response.
    // set evertyime to increase ttl
  }

  return storage.get(id) || {};
}

export async function setSession (data: SimpleObject, id: string): Promise<void>
export async function setSession (data: SimpleObject, cookie: Cookies): Promise<void>
export async function setSession (data: SimpleObject, cookieOrId: Cookies | string): Promise<void> {
  if(Object.keys(data).length === 0){
    return; 
  }
  const id = getId(cookieOrId);
  const ttl = Number(data.refresh_expires_in) || SESSION_TTL_AS_NUMBER //8h

  if(!await storage.set(id, data, ttl)) {
    throw new SessionException(id);
  }
 
}