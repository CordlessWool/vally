import { getElement} from "../elements/index";
import { createHash } from 'crypto';
import type { ElementData } from '../elements/types';
import fs from 'fs/promises';
import {join} from 'path';

export interface computeOptions {
  fetch: typeof fetch
}

export const prepareCompute = ({fetch}: computeOptions) => async function compute (data: ElementData): Promise<ElementData> {
  const element = getElement(data.type);
  if(element.beforeBuild) {
    return element.beforeBuild(data, {compute, storeAsset, fetch});
  }

  return data
}

export const storeAsset = async (content: Buffer, name: string, fileType: string): Promise<string> => {
  const queryHash = createHash('sha1').update(JSON.stringify(content)).digest('hex');
  const path = `/file_/${name}-${queryHash}.${fileType}`;
  await fs.writeFile(join('./static', path), content);
  
  return path;
}