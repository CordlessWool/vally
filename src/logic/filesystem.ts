import type { Template } from '@types';
import fs from 'fs/promises';
import path from 'path';
import type { PageFile } from '@types';

type filter<T> = (data: T) => boolean;


export const PAGES_PATH = './content/pages';
export const TEMPLATE_PATH = './templates';
export const IMAGE_PATH = './content/images';
export const FILE_PATH = './content/files';

export const readJsonFromFilesytem = async <T = JSON>(basePath: string, filter: filter<T> = () => true, deep: boolean | number = 0,): Promise<T[]> =>  {
  
  let all: T[] = []
  const resolvePath = basePath;//path.resolve(basePath);

  const pathContent = await fs.readdir(resolvePath, {withFileTypes: true});

  await Promise.all(pathContent.map(async (dir) => {

    if(dir.isFile() && dir.name.endsWith('.json')) {
      const json = await getJson(basePath, dir.name);

      if(filter(json)) {
        all.push(json);
      }
    } else if ((deep === true || deep > 0) && dir.isDirectory()) {

      const reduced = Number.isInteger(deep) ? <number>deep-1 : deep;

      const sub = await readJsonFromFilesytem<T>(path.join(resolvePath, dir.name), filter, reduced);
      all = all.concat(sub);
    }
    
  }));

  return all;
}

export const listDir = async (basePath: string): Promise<string[]> => {
  const pathContent = await fs.readdir(basePath, {withFileTypes: true});

  const files = await Promise.all(pathContent.map(async (dir): Promise<string | string[]> => {

    if (dir.isDirectory()) {
      const list =  await listDir(path.join(basePath, dir.name));
      return list.map((name) => path.join(dir.name, name));
    }

    return dir.name;
    
  }));

  return files.flat();
}


export const getJson = async (pathString: string, filename: string) => {
  const dataString = (await fs.readFile(path.resolve(pathString, filename), 'utf8')).trim();
  return JSON.parse(dataString);
}

export const saveJson = async (pathString: string, filename: string, data: unknown) => {
  return fs.writeFile(path.resolve(pathString, filename), JSON.stringify(data), 'utf-8')
}

export const deleteJson = async (pathString: string, filename: string) => {
  return fs.rm(path.resolve(pathString, filename));
}

export const getPage = async (filename: string): Promise<PageFile> => {
  return getJson(PAGES_PATH, `${filename}.json`);
}

export const savePage = async (filename: string, data: PageFile) => {
  return saveJson(PAGES_PATH, `${filename}.json`, data);
}

export const hasPage = async (filename: string): Promise<boolean> => {
  const pages = await getListOfPages();
  return pages.includes(`${filename}.json`)
}

export const deletePage = async (filename: string): Promise<void> => {
  deleteJson(PAGES_PATH, `${filename}.json`)
}

export const getListOfPages = async (): Promise<string[]> => {
  return (await fs.readdir(PAGES_PATH)).filter(name => name.endsWith('.json'));
}


/**
 * @deprecated Will be handled via seprate file
 * 
 * @param name 
 * @returns 
 */
export const getTemplatesByName = async (...name: string[]) => {
  //TODO: Buffer in Redis
  const templates = await getAllTemplates();
  return templates.filter((t) => name.includes(t.name));
}

/**
 * @deprecated Will be handled via seprate file
 * 
 */
export const getAllTemplates = async (): Promise<Template[]> => {
  return readJsonFromFilesytem<Template>(TEMPLATE_PATH, ({type}) => type === 'Template', 2);
}

export const saveFile = async (file: File) => {
  const data = await file.arrayBuffer();
  await fs.writeFile(path.resolve(FILE_PATH, file.name), Buffer.from(data));
  return file.name;
}

export const saveImage = async (file: File): Promise<string> => {
  const data = await file.arrayBuffer();
  await fs.writeFile(path.resolve(IMAGE_PATH, file.name), Buffer.from(data));
  return file.name;
}

export const getImage = async (name: string): Promise<Buffer> => {
  return fs.readFile(path.resolve(IMAGE_PATH, name));
}