
export interface ContrastColorProps {
  handler?: 'contrastColor',
  value: string,
}


export const contrastColor = ({value: color}: ContrastColorProps): 'black'|'white' => {
  
  if(typeof color !== 'string' || (color.length !== 4 && color.length !== 7)){
    throw new Error('Calculation of contrast color faild!');
  }
  
  let green: string, 
      blue: string, 
      red: string;
    
  const hex = [...color];
  if(hex.length === 4){
    red = "0x"+hex[1]+hex[1];
    green = "0x"+hex[2]+hex[2];
    blue = "0x"+hex[3]+hex[3];   
  } else {
    red = "0x"+hex[1]+hex[2];
    green = "0x"+hex[3]+hex[4];
    blue = "0x"+hex[5]+hex[6];
  }
  
  const sum = Math.round(((parseInt(red, 16) * 299) + (parseInt(green, 16) * 587) + (parseInt(blue, 16 ) * 114)) / 1000);
  return (sum > 128) ? 'black' : 'white';
};