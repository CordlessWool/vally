import { listDir } from './filesystem';
import fs from 'fs/promises';
import { resolve, join, dirname } from 'path';

import { faker } from '@faker-js/faker';


const TEST_BASE_PATH = 'jl19203123jlkj1lob231hl2';


async function createFiles(base: string, paths: string[]) {
  return Promise.all(paths.map(async (path) => {
      const fullPath = join(base, path);
      await fs.mkdir(dirname(fullPath), {recursive: true})
      await fs.writeFile(fullPath, faker.random.alphaNumeric())
    }
  ))
}

describe('Test function to access Filesystem', () => {
  beforeAll(async () => {
    await fs.mkdir(TEST_BASE_PATH);
  })

  afterAll(async () => {
    await fs.rm(TEST_BASE_PATH, {force: true, recursive: true});
  })


  describe('listDir', () => {

    const base = resolve(TEST_BASE_PATH, 'listDir');
    const testPaths = [
      'test/some/text.txt',
      'have/some/rich.md',
      'get/some/other/level/of/deepness/in.html',
      'maybe/something/.hidden/in/a/folder.ts',
      'index.html',
      'app.css',
      'images/empty.webp'
    ]

    beforeAll(async () => {
      await fs.mkdir(base, {recursive: true});
      for(let i = 0; i < 7; i++){
        testPaths.push(faker.system.filePath().replace('/', ''));
      }
      await createFiles(base, testPaths);
      
    })

    afterAll(async () => {
      await fs.rm(base, {force: true, recursive: true})
    })

    it('get list', async () => {
      const list = await listDir(base);
      expect(list).toEqual(testPaths.sort());
    })


  })  
})

