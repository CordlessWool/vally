export interface KeepVisibleOptions {
	class?: string;
	translation?: [number, number] | number;
}

function calcTransform (node: HTMLElement, params?: KeepVisibleOptions) {
	const { translation = 0 } = params || {};

	const wWidth = window.innerWidth
	const wHeight = window.innerHeight;

	const rect = node.getBoundingClientRect();
	let translateX: number;
	let translateY: number;

	if(Array.isArray(translation)) {
		[translateX, translateY] = translation;
	} else {
		translateX = translation;
		translateY = translation;
	}

	let xOffset = 0;
	let yOffset = 0;

	if(rect.top < 0) {
		yOffset = Math.ceil((rect.height * translateY) + (rect.top * -1));
	} else if (rect.bottom > wHeight) {
		yOffset = Math.ceil((rect.height * translateY) + (wHeight - rect.bottom))
	}

	if(rect.left < 0) {
		xOffset = Math.ceil((rect.width * translateX) + (rect.left * -1));
	} else if (rect.right > wWidth) {
		xOffset = Math.ceil((rect.width * translateX) + ((wWidth - rect.right)));
	}


	if(yOffset != 0 && xOffset != 0) {
		node.style.transform = `translate(${xOffset}px, ${yOffset}px)`
	} else if(xOffset != 0) {
		node.style.transform = `translate(${xOffset}px, ${translateX * 100}%)`
	} else if(yOffset != 0) {
		node.style.transform = `translate(${translateY * 100}%, ${yOffset}px)`
	}
}

export function keepVisible (node: HTMLElement,  params?: KeepVisibleOptions): SvelteActionReturnType {
	
	calcTransform(node, params);

	const resizeObserver = new ResizeObserver((entires: ResizeObserverEntry[]) => {
		entires.forEach((entry) => {
			if(entry.contentRect) {
				calcTransform(node, params);
			}
		})
	})

	resizeObserver.observe(node);

	return {
		destroy() {
			resizeObserver.disconnect();
		}
	}
}