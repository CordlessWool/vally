import { Subject, mergeAll, Observable, fromEvent, startWith, map, switchMap, last, takeUntil, filter, tap, take, ReplaySubject, mergeWith } from "rxjs";

interface MouseCordinates {
  originalEvent?: MouseEvent,
  deltaX: number,
  deltaY: number,
  startOffsetX: number,
  startOffsetY: number
}

export interface DragData {
  type: string,
  data: unknown
}


const droped$ = new ReplaySubject<Observable<DragData>>();
const drag$ = new Subject<Observable<DragData>>();
const drags: Observable<DragData>[] = []
const dragging$ = new Subject<Observable<MouseCordinates>>();

export function makeDraggable(element: HTMLElement, type='internal', data: undefined | unknown={}) {
  
  const { dragMove$, dragStart$, dragEnd$ } = createDragEvents(element, type, data);

  dragging$.next(dragMove$);
  droped$.next(dragEnd$);
  drag$.next(dragStart$);
  drags.push(dragStart$)

  return { dragMove$, dragStart$, dragEnd$ };

}

export function getDrags() {
  return {
    dragStart$: drag$.pipe(
      mergeAll(),
      mergeWith(...drags)
    ),
    dragMove$: dragging$.pipe(mergeAll()),
    dragEnd$: droped$.pipe(mergeAll()),
  }
}

function createDragEvents(element: HTMLElement, type: string, data: unknown) {
  const mouseUp$ = fromEvent<MouseEvent>(document, 'mouseup');
  const mouseMove$ = fromEvent<MouseEvent>(document, 'mousemove');
  const mouseDown$ = fromEvent<MouseEvent>(element, 'mousedown');

  const MIN_DELTA = 10;
  const dragStart$ = mouseDown$.pipe(
    filter((event) => event.button === 0),
    tap((event) => {event.stopPropagation()}),
    switchMap(start =>
      mouseMove$.pipe(
        startWith(start),
        filter((move) => Math.abs(move.pageX - start.pageX) > MIN_DELTA || Math.abs(move.pageY - start.pageY) > MIN_DELTA),
        takeUntil(mouseUp$),
        take(1),
        tap((/* evnet */) =>
          element.dispatchEvent(new CustomEvent('dragstart'))
        ),
      )
    )
  );

  const dragMove$ = dragStart$.pipe(
    switchMap(start =>
      mouseMove$.pipe(
        startWith(start),
        map(toDragEvent(start)),
        takeUntil(mouseUp$)
        
      )
    )
  );

  const dragEnd$ = dragStart$.pipe(
    switchMap(start =>
      mouseMove$.pipe(
        startWith(start),
        takeUntil(mouseUp$),
        last(),
        map(() => ({type, data})),
        tap((/* evnet */) =>
          element.dispatchEvent(new CustomEvent('dragend'))
        ),
      )
    )
  );

  return {
    dragStart$: dragStart$.pipe(map(() => ({type, data}))),
    dragEnd$,
    dragMove$
  };
}


function toDragEvent(start: MouseEvent) {
    const startOffsetX = start.clientX;
    const startOffsetY = start.clientX;
  return (moveEvent: MouseEvent): MouseCordinates => ({
    originalEvent: moveEvent,
    deltaX: moveEvent.pageX - start.pageX,
    deltaY: moveEvent.pageY - start.pageY,
    startOffsetX,
    startOffsetY
  });
}
