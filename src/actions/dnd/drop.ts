import { getDrags, type DragData } from "./store";

export interface DropZoneOptions {
  types: string[]
}


export function dropZone (node: HTMLElement, options: DropZoneOptions): SvelteActionReturnType {
  const {dragStart$, dragEnd$, dragMove$} = getDrags()
  const {types} = options;
  const attachedListeneders: Array<[string, (...data: unknown[]) => void]> = []
  let isOver: string | null = null;

  const handleEnter = (data: DragData) => {
    const event = new CustomEvent('dragenter', {detail: data})
    return () => {
      isOver = data.type;
      node.dispatchEvent(event);
    } 
  }

  const handleLeave = (data: DragData) => {
    const event = new CustomEvent('dragleave', {detail: data})
    return () => {
      isOver = null;
      node.dispatchEvent(event);
    }
  }

  // node.addEventListener('mouseover', handleEnter);
  //     node.addEventListener('mouseleave', handleLeave);
  const handleDrop = (data: DragData) => {
    if(isOver === data.type) {
      node.dispatchEvent(new CustomEvent('drop', {detail: data}))
      isOver = null;
    }
    attachedListeneders.forEach(([event, fu]) => {
      node.removeEventListener(event, fu);
    })
  }


  const dragSubscription = dragStart$.subscribe(({type, data}) => {
    if(types.length === 0 || types.includes(type)){
      const enter = handleEnter({type, data});
      const leave = handleLeave({type, data})
      attachedListeneders.push(['mouseenter', enter]);
      attachedListeneders.push(['mouseleave', leave])
      node.addEventListener('mouseenter', enter);
      node.addEventListener('mouseleave', leave);
    }
  });

  const dropSubscription = dragEnd$.subscribe(handleDrop);

  


  return {
    destroy() {
      dragSubscription.unsubscribe();
      dropSubscription.unsubscribe();
    }
  }

}