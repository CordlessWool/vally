import { makeDraggable } from "./store";

export interface DragOptions {
  shadow?: boolean,
  type?: string;
  data?: unknown;
  active?: boolean;
  initSelector?: string;
}

export function dragAble(node: HTMLElement, options: DragOptions){
  const {type, data, initSelector, active = true} = options;
  if(active === true) {
    const htmlInitator = initSelector == null ? node : <HTMLElement>node.querySelector(initSelector);
    const {dragStart$, dragMove$, dragEnd$} = makeDraggable(htmlInitator, type, data)

    const movement = {
      x: 0,
      y: 0
    }

    dragMove$.subscribe((pos) => {
      const page = {
        x: (pos.originalEvent?.pageX || 0),
        y: (pos.originalEvent?.pageY || 0)
      }
      const scroll = {
        x: window.scrollX,
        y: window.scrollY
      }

      if(page.y - 100 < scroll.y) {
        window.scrollTo({
          top: scroll.y - 100 / (page.y - scroll.y)
        })
      } else if (page.y + 100 > scroll.y + window.innerHeight) {
        window.scrollTo({
          top: scroll.y +  (page.y - scroll.y) / 100
        })
      }

      movement.x += page.x - (htmlInitator.getBoundingClientRect().left + scroll.x) - 20;// - pos.startOffsetX/2;
      movement.y += page.y - (htmlInitator.getBoundingClientRect().top + scroll.y) - 20;// - pos.startOffsetY/2;
      htmlInitator.style.transform = `scale(0.5) translate(${movement.x}px, ${movement.y}px)`;
    })

    dragEnd$.subscribe(() => {
      movement.x = 0;
      movement.y = 0;
      htmlInitator.style.transform = "";
    })
  }


}