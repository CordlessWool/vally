import { sequence } from "@sveltejs/kit/hooks";
import type { Handle } from "@sveltejs/kit";

import { env } from "$env/dynamic/private";
import { handleSession } from "./hooks/session";



const activeHandlers: Handle[] = [handleSession]

if(env.OPENID_PROVIDER_URL != null) {
  console.info('Register OPEN_ID login handler')
  const { handleAuth } = await import("./hooks/auth");
  activeHandlers.push(handleAuth)
}

export const handle = sequence(handleSession)