import type { Handle } from "@sveltejs/kit";
import { getSession, setSession } from "@logic/sessions";

export const handleSession: Handle = async ({event, resolve}) => {

  event.locals.session = getSession(event.cookies);
  const response = await resolve(event)
  await setSession(event.locals.session, event.cookies);

  return response;
}
