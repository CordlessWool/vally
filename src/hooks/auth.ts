import type { Handle } from "@sveltejs/kit";
import { isAuthenticated, refreshToken } from "../routes/auth/openid/client";

export const handleAuth: Handle = async ({event, resolve}) => {
  const {url, locals} = event;
  if(url.pathname.startsWith('/build')){
    console.info('require authentication')
    if(await isAuthenticated(locals.session)){
      console.info('is Authenticated');
      await refreshToken(locals.session); //TODO: fresh token also on other requests
      return resolve(event);
    }
    url.pathname = '/auth';
    return Response.redirect(url, 307);
  }
  console.info('no authentication required')
  const response = await resolve(event);

  

  return response;
}