import type { ScaleParams } from 'svelte/transition';
import { sineOut } from 'svelte/easing';

export function scaleY(node: Element, params: ScaleParams) {
  const existingTransform = getComputedStyle(node).transform.replace('none', '');

  return {
    delay: params.delay || 150,
    duration: params.duration || 500,
    easing: params.easing || sineOut,
    css: (t: number) => `transform: ${existingTransform} translateY(${-50+(50*t)}%) scaleY(${t})`
  };
}