import nodeAdapater from '@sveltejs/adapter-node';
import staticAapter from '@sveltejs/adapter-static';
import preprocess from 'svelte-preprocess';

// import { ENV } from '$env/dynamic/private'

/** @type {import('@sveltejs/kit').Config} */
const config = {
	// Consult https://github.com/sveltejs/svelte-preprocess
	// for more information about preprocessors
	preprocess: preprocess({
    postcss: true,
    typescript: true,
    sourceMap: true
  }),
	kit: {
		adapter: process.env.BUILD_FOR === 'node' ? nodeAdapater({
			precompress: true
		}) : staticAapter({
			strict: false,
			precompress: true,
		}),
		alias: {
			"@elements": "src/elements",
      "@comp": "src/components",
      "@templates": "templates",
			"@logic": "src/logic",
			"@lib": "src/lib",
			"@actions": "src/actions",
			"@types": "src/types"
		}
	},

	
};

export default config;
