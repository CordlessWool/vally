import { sveltekit } from '@sveltejs/kit/vite';

/** @type {import('vite').UserConfig} */
const config = {
  plugins: [sveltekit()],
	test: {
		globals: true,
		setupFiles: ['./test/vitest.setup.js']
		// enviroment: "jsdom"
	}

};

export default config;